package mx.dragonflylabs.library.interfaces;

import retrofit.RetrofitError;
import retrofit.client.Response;

public interface OnRetrofitResponse<T> {

	public void onSuccess(T object, Response retrofitResponse);
	public void onError(RetrofitError error);
	
}
