package mx.dragonflylabs.library;

import com.devspark.appmsg.AppMsg;
import com.dragonflylabs.dragonflylabslibrary.R;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;


public class DLBaseFragment extends Fragment{

protected ProgressDialog DL_PROGRESS_DIALOG;
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}
	
	protected void showProgressDialog(String msg, String title){
		DL_PROGRESS_DIALOG = new ProgressDialog(getActivity());
		DL_PROGRESS_DIALOG.setCancelable(false);
		DL_PROGRESS_DIALOG.setCanceledOnTouchOutside(false);
		DL_PROGRESS_DIALOG.setMessage(msg);
		DL_PROGRESS_DIALOG.setTitle(title);
		DL_PROGRESS_DIALOG.show();
	}
	
	protected void showProgressDialog(int msg_resource, int title_resource){
		DL_PROGRESS_DIALOG = new ProgressDialog(getActivity());
		DL_PROGRESS_DIALOG.setCancelable(false);
		DL_PROGRESS_DIALOG.setCanceledOnTouchOutside(false);
		DL_PROGRESS_DIALOG.setMessage(getString(msg_resource));
		DL_PROGRESS_DIALOG.setTitle(getString(title_resource));
		DL_PROGRESS_DIALOG.show();
	}
	
	protected void showLoadingDialog(){
		DL_PROGRESS_DIALOG = new ProgressDialog(getActivity());
		DL_PROGRESS_DIALOG.setCancelable(false);
		DL_PROGRESS_DIALOG.setCanceledOnTouchOutside(false);
		DL_PROGRESS_DIALOG.setMessage(getString(R.string.dl__dialog_loading));
		DL_PROGRESS_DIALOG.show();
	}
	
	protected void stopLoadingDialog(){
		stopProgressDialog();
	}
	
	protected void stopProgressDialog(){
		if(DL_PROGRESS_DIALOG != null && DL_PROGRESS_DIALOG.isShowing())
			DL_PROGRESS_DIALOG.dismiss();
	}
	
	protected boolean isConnected(){
		ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	}
	
	protected void showAlertMessage(int string_resource){
		AppMsg.makeText(getActivity(), getString(string_resource), AppMsg.STYLE_ALERT).show();
	}
	
	protected void showSuccessMessage(int string_resource){
		AppMsg.makeText(getActivity(), getString(string_resource), AppMsg.STYLE_CONFIRM).show();
	}
	
	protected void showInfoMessage(int string_resource){
		AppMsg.makeText(getActivity(), getString(string_resource), AppMsg.STYLE_INFO).show();
	}
	
	protected void showInfoMessage(String msg){
		AppMsg.makeText(getActivity(), msg, AppMsg.STYLE_INFO).show();
	}
	
	protected void showAlertMessage(String msg){
		AppMsg.makeText(getActivity(), msg, AppMsg.STYLE_ALERT).show();
	}
	
	protected void showSuccessMessage(String msg){
		AppMsg.makeText(getActivity(), msg, AppMsg.STYLE_CONFIRM).show();
	}
	
	protected void showAlertMessage(int string_resource_format, Object... params){
		
		AppMsg.makeText(getActivity(), getString(string_resource_format, params), AppMsg.STYLE_ALERT).show();
	}
	
	protected void showSuccessMessage(int string_resource_format, Object... params){
		AppMsg.makeText(getActivity(), getString(string_resource_format, params), AppMsg.STYLE_CONFIRM).show();
	}
	
	protected void showInfoMessage(int string_resource_format, Object... params){
		AppMsg.makeText(getActivity(), getString(string_resource_format, params), AppMsg.STYLE_INFO).show();
	}
	
	protected void showAlertCroutonMessage(int string_resource){
		Crouton.makeText(getActivity(), getString(string_resource), Style.ALERT).show();
	}
	
	protected void showSuccessCroutonMessage(int string_resource){
		Crouton.makeText(getActivity(), getString(string_resource), Style.CONFIRM).show();
	}
	
	protected void showInfoCroutonMessage(int string_resource){
		Crouton.makeText(getActivity(), getString(string_resource), Style.INFO).show();
	}
	
	protected void showInfoCroutonMessage(String msg){
		Crouton.makeText(getActivity(), msg, Style.INFO).show();
	}
	
	protected void showAlertCroutonMessage(String msg){
		Crouton.makeText(getActivity(), msg, Style.ALERT).show();
	}
	
	protected void showSuccessCroutonMessage(String msg){
		Crouton.makeText(getActivity(), msg, Style.CONFIRM).show();
	}
	
	protected void showAlertCroutonMessage(int string_resource_format, Object... params){
		Crouton.makeText(getActivity(), getString(string_resource_format, params), Style.ALERT).show();
	}
	
	protected void showSuccessCroutonMessage(int string_resource_format, Object... params){
		Crouton.makeText(getActivity(), getString(string_resource_format, params), Style.CONFIRM).show();
	}
	
	protected void showInfoCroutonMessage(int string_resource_format, Object... params){
		Crouton.makeText(getActivity(), getString(string_resource_format, params), Style.INFO).show();
	}
	
	protected void showDialogMessage(String msg, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener){
		AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity()).setMessage(msg).setPositiveButton(getString(R.string.dl__dialog_confirm), positiveListener)
				.setNegativeButton(getString(R.string.dl__dialog_cancel), negativeListener);
		dialog.create().show();
	}
	
	@Override
	public void onDestroyView() {
		AppMsg.cancelAll();
		super.onDestroyView();
	}
	
	protected void launchActivity(Class <?> clss){
		Intent i = new Intent(getActivity(), clss);
		startActivity(i);
	}
	
	protected void d(Object obj){
		System.out.println(obj);
	}
}
